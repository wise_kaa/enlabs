create table user_account_log
(
    id             serial not null
        constraint user_account_log_pk
            primary key,
    amount         numeric,
    created_at     timestamp default CURRENT_TIMESTAMP,
    transaction_id varchar(100),
    account_id     integer
        constraint user_account_log_user_account_id_fk
            references user_account,
    num            integer   not null default 1,
    status         varchar   default 'approved'::character varying
);

create unique index user_account_log_transaction_id_uindex
    on user_account_log (transaction_id);
