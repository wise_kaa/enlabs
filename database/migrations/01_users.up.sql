create table users
(
    id   serial not null
        constraint users_pk
            primary key,
    name varchar(100)
);

insert into users (name) values ('John');
