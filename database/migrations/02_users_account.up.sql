CREATE TYPE account_type AS ENUM ('game', 'server', 'payment');

create table user_account
(
    id           serial            not null
        constraint user_account_pk
            primary key,
    user_id      integer           not null
        constraint user_account_users_id_fk
            references users,
    account_type account_type,
    balance      numeric default 0 not null
);

insert into user_account (user_id, account_type) values (1, 'game');
