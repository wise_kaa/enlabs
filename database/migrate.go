package database

import (
	"github.com/golang-migrate/migrate"
	"gitlab.com/wise_kaa/enlabs/configuration"

	_ "github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	"log"
)

func applyMigrations() {
	dbConf := configuration.GetConfig().DB
	m, err := migrate.New("file://"+dbConf.MigrationsPath, GetDatabaseURL())
	defer closeMigrations(m)

	if err != nil {
		log.Fatalf("Error while migrating: %s\n", err.Error())
	}
	if err := m.Up(); err != nil {
		log.Printf("Migration: %s", err)
	}
	log.Printf("Migrated\n")
}

func closeMigrations(m *migrate.Migrate) {
	if sErr, dbErr := m.Close(); sErr != nil {
		panic(sErr)
	} else if dbErr != nil {
		panic(dbErr)
	}
}
