package database

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/wise_kaa/enlabs/configuration"
)

var (
	Conn *sqlx.DB
)

func Init() error {

	dbConf := configuration.GetConfig().DB

	if dbConf.MigrateOnStart {
		applyMigrations()
	}

	Conn = NewConnection()
	return nil
}

func GetDatabaseURL() string {
	dbConf := configuration.GetConfig().DB
	source := fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=%s",
		dbConf.User,
		dbConf.Password,
		dbConf.Host,
		dbConf.Port,
		dbConf.DBName,
		dbConf.SSLMode,
	)

	return source
}

func NewConnection() *sqlx.DB {
	db, err := sqlx.Open("postgres", GetDatabaseURL())
	if err != nil {
		panic(err)
	}

	return db
}
