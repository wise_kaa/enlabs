package structs

import (
	"fmt"
	"github.com/shopspring/decimal"
)

type EventState string

const (
	EventStateWin  EventState = "win"
	EventStateLost EventState = "lost"
)

type GameCompleteRequest struct {
	State         EventState      `json:"state"`
	Amount        decimal.Decimal `json:"amount"`
	TransactionID string          `json:"transactionId"`
}

func (request GameCompleteRequest) Validate() (errors []string) {
	if request.State != EventStateWin && request.State != EventStateLost {
		errors = append(errors, fmt.Sprintf("invalid state: %s", request.State))
	}

	return
}
