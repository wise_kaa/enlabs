package game

import (
	"gitlab.com/wise_kaa/enlabs/pkg/repository"
	"gitlab.com/wise_kaa/enlabs/pkg/structs"
)

func GameComplete(request structs.GameCompleteRequest) error {
	return repository.AddAccountRec(request)
}
