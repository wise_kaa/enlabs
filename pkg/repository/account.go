package repository

import (
	"database/sql"
	"fmt"
	"github.com/lib/pq"
	"github.com/shopspring/decimal"
	"gitlab.com/wise_kaa/enlabs/database"
	"gitlab.com/wise_kaa/enlabs/pkg/structs"
)

func AddAccountRec(request structs.GameCompleteRequest) error {
	var userAccountID = 1 // temporary

	amount := request.Amount
	if request.State == structs.EventStateLost {
		amount = amount.Neg()
	}

	tx, err := database.Conn.Begin()
	if err != nil {
		return err
	}

	var balance decimal.Decimal
	err = tx.QueryRow(`
		SELECT 
			balance 
		FROM 
			user_account 
		WHERE 
			id = $1 
		FOR UPDATE;`, userAccountID).Scan(&balance)
	if err != nil {
		tx.Rollback()
		return err
	}

	// check for balance
	if balance.Add(amount).LessThan(decimal.Zero) {
		tx.Rollback()
		return fmt.Errorf("not enought balance")
	}

	_, err = tx.Exec(`
		INSERT INTO 
		    user_account_log (account_id, transaction_id, amount, num) 
		VALUES 
		    ($1,$2,$3, (SELECT coalesce(MAX(num)+1, 1) FROM user_account_log WHERE account_id = $1))`,
		userAccountID,
		request.TransactionID,
		amount,
	)

	if err != nil {
		switch errType := err.(type) {
		case *pq.Error:
			err = fmt.Errorf(errType.Detail)
		}

		tx.Rollback()
		return err
	}

	err = CalculateBalance(tx, userAccountID)
	if err != nil {
		tx.Rollback()
		return err
	}

	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()

	return nil
}

func CancelOddRec() error {
	var userAccountID = 1 // temporary
	tx, err := database.Conn.Begin()
	if err != nil {
		return err
	}

	_, err = tx.Exec(`
		UPDATE user_account_log
		SET status = 'canceled'
		WHERE id IN (
			SELECT id
			FROM user_account_log
			WHERE MOD(num, 2) = 1
			AND account_id = $1
			ORDER BY id DESC
			LIMIT 10
		);`, userAccountID)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = CalculateBalance(tx, userAccountID)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()

	return nil
}

func CalculateBalance(tx *sql.Tx, userAccountID int) error {
	_, err := tx.Exec(`
		UPDATE
			user_account
		SET
			balance = (
				SELECT COALESCE(SUM(amount), 0)
				FROM user_account_log
				WHERE account_id = $1
				AND status='approved')
		WHERE
			id = $1
		`,
		userAccountID,
	)

	return err
}

func GetAccBalance(userAccountID int) (*decimal.Decimal, error) {
	db := database.Conn

	var balance decimal.Decimal
	err := db.QueryRow(`
		SELECT 
			balance 
		FROM 
			user_account 
		WHERE 
			id = $1;`, userAccountID).Scan(&balance)
	if err != nil {
		return nil, err
	}

	return &balance, nil
}
