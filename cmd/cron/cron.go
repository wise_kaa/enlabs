package main

import (
	"gitlab.com/wise_kaa/enlabs/configuration"
	"gitlab.com/wise_kaa/enlabs/database"
	"gitlab.com/wise_kaa/enlabs/logs"
	"gitlab.com/wise_kaa/enlabs/pkg/repository"
	"log"
	"time"
)

func init() {
	// config
	if err := configuration.InitConfig(); err != nil {
		log.Fatal(err)
	}

	// logger
	if err := logs.Init(); err != nil {
		log.Fatal(err)
	}

	if err := database.Init(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	for {
		err := repository.CancelOddRec()
		logs.Log.Info("CancelOddRec processed")
		if err != nil {
			logs.Log.Error(err.Error())
		}
		time.Sleep(configuration.GetConfig().Settings.AccountCancelationInterval * time.Minute)
	}
}
