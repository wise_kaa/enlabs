package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/wise_kaa/enlabs/app"
	"gitlab.com/wise_kaa/enlabs/configuration"
	"gitlab.com/wise_kaa/enlabs/database"
	"gitlab.com/wise_kaa/enlabs/logs"
)

func init() {
	// config
	if err := configuration.InitConfig(); err != nil {
		log.Fatal(err)
	}

	// logger
	if err := logs.Init(); err != nil {
		log.Fatal(err)
	}

	if err := database.Init(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	router := app.Router()

	srv := &http.Server{
		Addr:    fmt.Sprint(configuration.GetConfig().HTTP.Host, ":", configuration.GetConfig().HTTP.Port),
		Handler: router,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shuting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
}
