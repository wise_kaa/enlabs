package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func HealthCheck(c *gin.Context) {
	c.String(http.StatusOK, "ok")
}

type ErrorResponse struct {
	State  string   `json:"state"`
	Errors []string `json:"errors"`
}

func ErrorResponseSingle(c *gin.Context, err error) {
	data := ErrorResponse{
		State:  "error",
		Errors: []string{err.Error()},
	}
	c.JSON(http.StatusBadRequest, data)
}

func ErrorResponseMulti(c *gin.Context, errors []string) {
	data := ErrorResponse{
		State:  "error",
		Errors: errors,
	}
	c.JSON(http.StatusBadRequest, data)
}
