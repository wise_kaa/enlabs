package controller

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/shopspring/decimal"
	"gitlab.com/wise_kaa/enlabs/pkg/game"
	"gitlab.com/wise_kaa/enlabs/pkg/repository"
	"gitlab.com/wise_kaa/enlabs/pkg/structs"
	"io/ioutil"
	"net/http"
)

func GameCompleteHandler(c *gin.Context) {
	var request structs.GameCompleteRequest

	data, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		ErrorResponseSingle(c, err)
		return
	}

	err = json.Unmarshal(data, &request)
	if err != nil {
		ErrorResponseSingle(c, err)
		return
	}

	if validationErrors := request.Validate(); len(validationErrors) > 0 {
		ErrorResponseMulti(c, validationErrors)
		return
	}

	err = game.GameComplete(request)
	if err != nil {
		ErrorResponseSingle(c, err)
		return
	}
	c.String(http.StatusOK, "ok")
}

func AccBalanceHandler(c *gin.Context) {
	var userAccountID = 1 // temporary

	balance, err := repository.GetAccBalance(userAccountID)
	if err != nil {
		ErrorResponseSingle(c, err)
		return
	}

	c.JSON(http.StatusOK, struct {
		Balance decimal.Decimal `json:"balance"`
	}{*balance},
	)
}
