package logs

import (
	"github.com/evalphobia/logrus_sentry"
	"github.com/sirupsen/logrus"
	"gitlab.com/wise_kaa/enlabs/configuration"
	"time"
)

var Log *logrus.Logger

func Init() error {
	Log = logrus.New()

	level, err := logrus.ParseLevel(configuration.GetConfig().Log.Level)
	if err != nil {
		return err
	}

	Log.SetLevel(level)

	initSentry(configuration.GetConfig().Log.Sentry, Log, "develop")

	return nil
}

func initSentry(sentryConfig *configuration.SentryConfig, log *logrus.Logger, stage string) {
	tags := map[string]string{
		"alex": "test1",
	}
	levels := []logrus.Level{
		logrus.ErrorLevel,
		logrus.InfoLevel,
	}
	hook, err := logrus_sentry.NewWithTagsSentryHook(sentryConfig.DSN, tags, levels)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Connected to Sentry on %s", sentryConfig.DSN)
	hook.SetEnvironment(stage)
	hook.Timeout = 10 * time.Second
	log.Hooks.Add(hook)
}
