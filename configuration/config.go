package configuration

import (
	config "github.com/microparts/configuration-golang"
	"gopkg.in/yaml.v2"
	"log"
	"time"
)

var appConfig *AppConfig

// AppConfig service config structure
type AppConfig struct {
	HTTP     httpConfig    `yaml:"http"`
	Formats  formatsConfig `yaml:"formats"`
	Log      LogConfig     `yaml:"log"`
	DB       DBConfig      `yaml:"db"`
	Settings settings      `yaml:"settings"`
}

type httpConfig struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

type formatsConfig struct {
	Time string `yaml:"time"`
	Date string `yaml:"date"`
}

type DBConfig struct {
	DriverName     string        `yaml:"driver"`
	User           string        `yaml:"user"`
	Password       string        `yaml:"password"`
	Host           string        `yaml:"host"`
	Port           int           `yaml:"port"`
	Schema         string        `yaml:"schema"`
	DBName         string        `yaml:"db"`
	SSLMode        string        `yaml:"ssl_mode"`
	MaxOpenConn    int           `yaml:"max_open_conn"`
	MaxIdleConn    int           `yaml:"max_idle_conn"`
	ConnLifeTime   time.Duration `yaml:"conn_life_time"`
	MigrateOnStart bool          `yaml:"migrate_on_start"`
	MigrationsPath string        `yaml:"migrations_path" binding:"required"`
}

type LogConfig struct {
	Level  string        `yaml:"level"`
	Format string        `yaml:"format"`
	Sentry *SentryConfig `yaml:"sentry,omitempty"`
}

type SentryConfig struct {
	Enable          bool          `yaml:"enable"`
	Stage           string        `yaml:"stage"`
	MinlLogLevel    string        `yaml:"min_log_level"`
	DSN             string        `yaml:"dsn"`
	ResponseTimeout time.Duration `yaml:"response_timeout"`
}

type settings struct {
	AccountCancelationInterval time.Duration `yaml:"account_cancelation_interval"`
}

// InitConfig initialize config data
func InitConfig() error {
	configPath := config.GetEnv("CONFIG_PATH", "")
	configBytes, err := config.ReadConfigs(configPath)
	if err != nil {
		log.Printf("[config] read error: %+v", err)
		return err
	}

	err = yaml.Unmarshal(configBytes, &appConfig)
	if err != nil {
		log.Printf("[config] unmarshal error for bytes: %+v", configBytes)
		return err
	}

	return nil
}

// GetConfig возвращает конфиг (глобальная переменная 'config' пакета 'configuration'),
// при необходимости инициализируя конфиг.
func GetConfig() *AppConfig {
	if appConfig == nil {
		if InitConfig() != nil {
			return nil
		}
	}

	return appConfig
}
