FROM golang:latest AS builder
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main ./cmd/myservice


FROM alpine

COPY --from=builder /app/main /
COPY /configuration /configuration
COPY /database/migrations /database/migrations

EXPOSE 8080

RUN chmod +x /main
CMD ["./main"]