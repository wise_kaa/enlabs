# test

The main goal of this test task is to develop the application for processing the incoming requests from the 3d-party providers.
The application must have an HTTP URL to receive incoming POST requests.
To receive the incoming POST requests the application must have an HTTP URL endpoint.

Technologies: Golang + Postgres.

Requirements:
1. Processing and saving incoming requests.

Imagine that we have a user with the account balance.

Example of the POST request:
POST /your_url HTTP/1.1
Source-Type: client
Content-Length: 34
Host: 127.0.0.1
Content-Type: application/json
{"state": "win", "amount": "10.15", "transactionId": "some generated identificator"}

Header “Source-Type” could be in 3 types (game, server, payment). This type probably can be extended in the future.

Possible states (win, lost):
1. Win requests must increase the user balance
2. Lost requests must decrease user balance.
Each request (with the same transaction id) must be processed only once.

The decision regarding database architecture and table structure is made to you.

You should know that account balance can't be in a negative value.
The application must be competitive ability.

2. Post-processing
Every N minutes 10 latest odd records must be canceled and balance should be corrected by the application.
Cancelled records shouldn't be processed twice.

3. The application should be prepared for running via docker containers.

Please be informed and kindly note that application without description about how to run and test won't be accepted and reviewed. 

---------
to start:

git clone git@gitlab.com:wise_kaa/enlabs.git 
docker-compose up

по адресу http://localhost:8080 будут доступны эндпоинты
1. 
POST http://localhost:8080/game_complete HTTP/1.1
Content-Type: application/json
Source-Type: game
cache-control: no-cache
{
    "state": "win",
    "amount": "4.45",
    "transactionId": "some generated identificator jmgfgkuitku"
}

2.
GET http://localhost:8080/get_account_balance
Source-Type: game
cache-control: no-cache

Постман коллекция в файле enlabs.postman_collection.json

Если порт 8080 занят, можно поменять в configuration/docker/http.yaml